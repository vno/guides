package com.nerchain.springwebsocketsstomp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebsocketsStompApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringWebsocketsStompApplication.class, args);
    }

}

